module modernc.org/yy

go 1.16

require (
	modernc.org/mathutil v1.5.0
	modernc.org/parser v1.0.7
	modernc.org/strutil v1.1.3
	modernc.org/y v1.0.9
)
